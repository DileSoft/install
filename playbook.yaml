---
- hosts: 127.0.0.1
  connection: local
  tasks:
    - name: Определение операционной системы
      setup:
        gather_subset: all
      register: facts

    - name: Установка EPEL-репозитория (только для CentOS)
      yum:
        name:
          - epel-release
          - yum-utils
          - device-mapper-persistent-data
          - lvm2
          - wget
          - git
        state: present
      when: "'CentOS' in facts.ansible_facts['ansible_distribution']"

    - name: Установка Docker (только для CentOS)
      yum:
        name: docker-ce
        state: present
      when: "'CentOS' in facts.ansible_facts['ansible_distribution']"

    - name: Добавление пользователя в группу docker (только для CentOS)
      user:
        name: "{{ ansible_user }}"
        groups: docker
        append: yes
      when: "'CentOS' in facts.ansible_facts['ansible_distribution']"

    - name: Установка зависимостей (только для Debian)
      apt:
        name:
          - apt-transport-https
          - ca-certificates
          - curl
          - gnupg2
          - software-properties-common
          - git
          - python3-pip
        state: present
      when: "'Debian' in facts.ansible_facts['ansible_distribution']"

    - name: Add the GPG key for Docker (только для Debian)
      shell: 'curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg'
      when: "'Debian' in facts.ansible_facts['ansible_distribution']"

    - name: Add the repository to fetch the docker package (только для Debian)
      shell: 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian bullseye stable" | tee /etc/apt/sources.list.d/docker.list'
      when: "'Debian' in facts.ansible_facts['ansible_distribution']"

    - name: Update source list and then install docker (только для Debian)
      apt: update_cache=yes name=docker-ce state=latest
      when: "'Debian' in facts.ansible_facts['ansible_distribution']"

    - name: Install the Docker module for Python, required by ansible (только для Debian)
      pip:
        name: docker
      when: "'Debian' in facts.ansible_facts['ansible_distribution']"
      
    # - name: Добавление пользователя в группу docker (только для Debian)
    #   user:
    #     name: "{{ ansible_user }}"
    #     groups: docker
    #     append: yes
    #   when: "'Debian' in facts.ansible_facts['ansible_distribution']"

    - name: Установка Docker Compose
      get_url:
         url: https://github.com/docker/compose/releases/latest/download/docker-compose-Linux-x86_64
         dest: /usr/bin/docker-compose
         mode: 'a+x'

    - name: Установка Node.js и npm
      package:
        name: "{{ item }}"
        state: present
      loop:
        - nodejs
        - npm
      when: "'Debian' in facts.ansible_facts['ansible_distribution'] or 'CentOS' in facts.ansible_facts['ansible_distribution']"

    - name: Установка ts-node через npm
      command: npm install -g ts-node

    - name: Запуск сервиса Docker
      service:
        name: docker
        state: started
        enabled: yes

    - name: Копирование файла Docker Compose
      copy:
        src: ./docker-file/docker-compose.yml
        dest: /opt/docker-compose.yml

    - name: Клонирование репозитория Core
      git:
        repo: "https://gitlab.com/megapolos-fork/megapolos-core.git"
        dest: "/opt/core"
        version: main
        force: yes

    - name: Клонирование репозитория GUI
      git:
        repo: "https://gitlab.com/megapolos-fork/megapolos-gui.git"
        dest: "/opt/gui"
        version: main
        force: yes

    - name: Сборка Docker-образа GUI
      command: docker build -t gui /opt/gui

    - name: Запуск Docker Compose
      command: docker-compose -f /opt/docker-compose.yml up -d

    # - name: Ожидание запуска сервисов gui
    #   wait_for:
    #     host: localhost
    #     port: 8080
    #     delay: 5
    #     timeout: 300

    - name: Установка модулей core
      command: npm install
      args:
        chdir: /opt/core

    - name: Генерировать токен
      command: "openssl rand -hex 16"
      register: token_output

    - name: Проверить наличие файла config.json
      stat:
        path: /opt/core/config/config.json
      register: config_file_stat

    - name: Создать директорию /opt/core/config
      file:
        path: /opt/core/config
        state: directory
        mode: "0755"
      when: not config_file_stat.stat.exists

    - name: Создать файл config.json
      copy:
        dest: /opt/core/config/config.json
        content: |
          {
            "secret": "{{ token_output.stdout }}"
          }
      when: not config_file_stat.stat.exists

    - name: Чтение файла config.json
      shell: cat /opt/core/config/config.json
      register: config_content

    - name: Вывести содержимое файла config.json
      debug:
        var: config_content.stdout

    - name: Создать файл юнита systemd
      copy:
        dest: /etc/systemd/system/megapolos-core.service
        content: |
          [Unit]
          Description=megapolos-core Service

          [Service]
          ExecStart=/usr/local/bin/ts-node /opt/core/install/install.ts

    - name: Перезапустить демон systemd
      systemd:
        name: megapolos-core.service
        state: restarted
        daemon_reload: yes

    - name: Ожидать старта сервиса
      systemd:
        name: megapolos-core.service
        state: started
        enabled: yes
      register: systemd_status
      retries: 5
      delay: 5

    - name: Вывести статус сервиса
      debug:
        var: systemd_status
